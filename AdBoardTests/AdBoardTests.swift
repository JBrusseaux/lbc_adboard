//
//  AdBoardTests.swift
//  AdBoardTests
//
//  Created by Julien Brusseaux on 03/03/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import XCTest

class AdBoardTests: XCTestCase {
    
    func testCategoriesReachable() {
        let expectation = self.expectation(description: "Task response")
        URLSession.shared.dataTask(with: AdLoader.shared.categoriesSource) { (data, response, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(data)
            XCTAssertNotNil(response)
            expectation.fulfill()
        }
        .resume()
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testAdsReachable() {
        let expectation = self.expectation(description: "Task response")
        URLSession.shared.dataTask(with: AdLoader.shared.adsSource) { (data, response, error) in
            XCTAssertNil(error)
            XCTAssertNotNil(data)
            XCTAssertNotNil(response)
            expectation.fulfill()
        }
        .resume()
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testServiceResponse() {
        let expectation = self.expectation(description: "Ads received")
        AdLoader.shared.loadAds { result in
            switch result {
            case let .success(ads): XCTAssert(!ads.isEmpty)
            case .failure: XCTFail()
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
    func testValidAdFormat() {
        let data = """
            {
               "id":1461742431,
               "category_id":2,
               "title":"Pull bleu coton",
               "description":"Pull anglais coton",
               "price":10.00,
               "images_url":{
                  "small":"https://raw.githubusercontent.com/leboncoin/paperclip/master/ad-small/df40ee665ce19053e5465c23180e9e9c75c4b71d.jpg",
                  "thumb":"https://raw.githubusercontent.com/leboncoin/paperclip/master/ad-thumb/df40ee665ce19053e5465c23180e9e9c75c4b71d.jpg"
               },
               "creation_date":"2019-11-05T15:56:33+0000",
               "is_urgent":true
            }
        """.data(using: .utf8)
        
        if let data = data {
            XCTAssertNotNil(try? JSONDecoder().decode(Ad.self, from: data))
        } else {
            XCTFail()
        }
    }
    
    func testInvalidAdFormat() {
        let data = """
            {
               "category_id":2,
               "title":"Pull bleu coton",
               "description":"Pull anglais coton",
               "price":10.00,
               "images_url":{
                  "small":"https://raw.githubusercontent.com/leboncoin/paperclip/master/ad-small/df40ee665ce19053e5465c23180e9e9c75c4b71d.jpg",
                  "thumb":"https://raw.githubusercontent.com/leboncoin/paperclip/master/ad-thumb/df40ee665ce19053e5465c23180e9e9c75c4b71d.jpg"
               },
               "creation_date":"2019-11-05T15:56:33+0000",
               "is_urgent":true
            }
        """.data(using: .utf8)
        
        if let data = data {
            XCTAssertNil(try? JSONDecoder().decode(Ad.self, from: data))
        } else {
            XCTFail()
        }
    }
    
    func testAdsSortedCorrectly() {
        let expectation = self.expectation(description: "Ads received")
        AdLoader.shared.loadAds { result in
            switch result {
            case let .success(ads):
                (0 ..< ads.count-1).forEach { index in
                    XCTAssert(ads[index].creationDate >= ads[index+1].creationDate || ads[index].urgent)
                }
            case .failure: XCTFail()
            }
            expectation.fulfill()
        }
        waitForExpectations(timeout: 5, handler: nil)
    }
    
}
