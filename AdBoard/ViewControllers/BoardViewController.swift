//
//  ViewController.swift
//  AdBoard
//
//  Created by Julien Brusseaux on 29/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import UIKit

class BoardViewController: UICollectionViewController {
    
    private var ads = [Ad]() {
        didSet {
            displayedAds = ads
        }
    }
    
    private var displayedAds = [Ad]() {
        didSet {
            DispatchQueue.main.async {
                self.collectionView.reloadData()
            }
        }
    }
    
    private var filterAlert: UIAlertController {
        let alert = UIAlertController(title: "Catégorie", message: nil, preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Toutes", style: .default, handler: { _ in
            self.displayedAds = self.ads
        }))
        Set(ads.compactMap { $0.category })
            .sorted(by: { $0.name < $1.name })
            .forEach { category in
                alert.addAction(UIAlertAction(title: category.name, style: .default, handler: { _ in
                    self.displayedAds = self.ads.filter { $0.category == category }
                }))
        }
        return alert
    }
    
    override func loadView() {
        loadCollectionView()
        navigationItem.setRightBarButton(UIBarButtonItem(title: "Filtrer", style: .plain, target: self, action: #selector(showFilterAlert)), animated: true)
        AdLoader.shared.loadAds { [weak self] result in
            switch result {
            case let .failure(error): self?.showError(error)
            case let .success(ads):
                self?.ads = ads
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        updateLayout()
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return displayedAds.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "AdCell", for: indexPath)
        (cell as? AdCell)?.setup(with: displayedAds[indexPath.item])
        return cell
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let detail = AdViewController()
        detail.setup(with: displayedAds[indexPath.item])
        navigationController?.pushViewController(detail, animated: true)
    }
}

private extension BoardViewController {
    func loadCollectionView() {
        let layout = UICollectionViewFlowLayout()
        collectionView = UICollectionView(frame: UIScreen.main.bounds, collectionViewLayout: layout)
        collectionView.register(AdCell.self, forCellWithReuseIdentifier: "AdCell")
        collectionView.backgroundColor = .white
    }
    
    func updateLayout() {
        guard let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else { return }
        
        let spacing: CGFloat = 8
        layout.minimumInteritemSpacing = spacing
        let columns = Int((collectionView.bounds.width-spacing)/(150+spacing))
        let columnWidth = collectionView.bounds.width/CGFloat(columns)-spacing
        layout.itemSize = CGSize(width: columnWidth, height: columnWidth+80)
    }
    
    func showError(_ error: Error) {
        DispatchQueue.main.async {
            let alert = UIAlertController(title: nil, message: error.localizedDescription, preferredStyle: .alert)
            alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    @objc func showFilterAlert() {
        present(filterAlert, animated: true)
    }
}
