//
//  AdViewController.swift
//  AdBoard
//
//  Created by Julien Brusseaux on 03/03/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import UIKit

class AdViewController: UIViewController {
    
    private static let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "fr_FR")
        formatter.currencySymbol = "€"
        formatter.usesSignificantDigits = true
        return formatter
    } ()
    
    private static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "d MMMM yyyy"
        formatter.locale = Locale(identifier: "fr_FR")
        return formatter
    } ()
    
    private let scrollView = UIScrollView()
    private let titleView = UIView()
    private let titleLabel = UILabel()
    private let descriptionLabel = UILabel()
    private let dateLabel = UILabel()
    private let priceLabel = UILabel()
    private let adImageView = UIImageView()
    private let categoryView = UIView()
    private let categoryLabel = UILabel()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.with(
            scrollView.with(
                adImageView,
                titleView.with(
                    categoryView.with(
                        categoryLabel),
                    titleLabel,
                    priceLabel),
                descriptionLabel,
                dateLabel)
        )
        loadConstraints()
        loadStyle()
    }
    
    func setup(with ad: Ad) {
        titleLabel.text = ad.title
        dateLabel.text = "Publiée le " + AdViewController.dateFormatter.string(from: ad.creationDate)
        priceLabel.text = AdViewController.currencyFormatter.string(from: NSNumber(value: ad.price))
        categoryLabel.text = ad.category?.name
        descriptionLabel.text = ad.description
        adImageView.image = nil
        
        guard let path = ad.thumbnail, let url = URL(string: path) else { return }
        let imageLoadTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, _, _) in
            if let data = data {
                DispatchQueue.main.async { [weak self] in
                    self?.adImageView.image = UIImage(data: data)
                    if let adImageView = self?.adImageView, let image = adImageView.image {
                        adImageView.heightAnchor.constraint(equalTo: adImageView.widthAnchor,
                            multiplier: image.size.height/image.size.width).isActive = true
                    }
                }
            }
        })
        imageLoadTask.resume()
    }
    
    func loadConstraints() {
        scrollView.constrain(to: view)
        adImageView.constrain(to: scrollView, attributes: [.leading, .trailing, .top])
        adImageView.widthAnchor.constraint(equalTo: scrollView.widthAnchor).isActive = true
        adImageView.placeOnTop(of: titleView, offset: 8)
        titleView.constrain(to: scrollView, attributes: [.leading, .trailing], constants: [12, 12])
        categoryView.constrain(to: titleView, attributes: [.leading, .top])
        categoryView.placeOnTop(of: titleLabel, offset: 4)
        titleLabel.constrain(to: titleView, attributes: [.leading, .bottom])
        priceLabel.constrain(to: titleView, attributes: [.top, .trailing, .bottom])
        titleLabel.placeNext(to: priceLabel, offset: 8)
        titleView.placeOnTop(of: descriptionLabel, offset: 16)
        descriptionLabel.constrain(to: titleView, attributes: [.leading, .trailing])
        descriptionLabel.placeOnTop(of: dateLabel, offset: 16)
        dateLabel.constrain(to: descriptionLabel, attributes: [.leading, .trailing])
        scrollView.bottomAnchor.constraint(equalTo: dateLabel.bottomAnchor, constant: 16).isActive = true
    }
    
    func loadStyle() {
        view.backgroundColor = .white
        
        adImageView.backgroundColor = .lightGray
        adImageView.contentMode = .scaleAspectFit
        
        titleLabel.numberOfLines = 0
        titleLabel.textColor = .black
        titleLabel.font = UIFont.boldSystemFont(ofSize: 18)
        
        dateLabel.textColor = .gray
        dateLabel.font = UIFont.systemFont(ofSize: 13, weight: .medium)
        dateLabel.textAlignment = .center
        
        priceLabel.font = UIFont.boldSystemFont(ofSize: 20)
        priceLabel.textColor = UIColor(red: 0.1, green: 0.811, blue: 0.117, alpha: 1)
        priceLabel.setContentCompressionResistancePriority(.required, for: .horizontal)
        priceLabel.setContentHuggingPriority(.required, for: .horizontal)
        
        categoryLabel.font = UIFont.boldSystemFont(ofSize: 13)
        categoryLabel.superview?.backgroundColor = UIColor(red: 0.989, green: 0.411, blue: 0.225, alpha: 1)
        categoryLabel.superview?.layer.cornerRadius = 8
        
        descriptionLabel.numberOfLines = 0
        descriptionLabel.textColor = .black
    }
}
