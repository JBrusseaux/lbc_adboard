//
//  AdCategory.swift
//  AdBoard
//
//  Created by Julien Brusseaux on 29/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import Foundation

class AdCategory: Codable {
    let id: Int
    let name: String
}

extension AdCategory: Hashable {
    static func == (lhs: AdCategory, rhs: AdCategory) -> Bool {
        return lhs.id == rhs.id
    }
    
    func hash(into hasher: inout Hasher) {
        hasher.combine(id)
    }
}
