//
//  Ad.swift
//  AdBoard
//
//  Created by Julien Brusseaux on 29/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import Foundation

class Ad: Codable {
    fileprivate static let dateFormatter: ISO8601DateFormatter = {
        let formatter = ISO8601DateFormatter()
        formatter.formatOptions = [.withInternetDateTime]
        formatter.timeZone = TimeZone(secondsFromGMT: 0)
        return formatter
    } ()

    let id: Int
    let categoryId: Int
    let title: String
    let description: String
    let price: Double
    let creationDate: Date
    let urgent: Bool

    let image: String?
    let thumbnail: String?
    
    var category: AdCategory? = nil
    
    enum CodingKeys: String, CodingKey {
        case id
        case categoryId = "category_id"
        case title
        case description
        case price
        case creationDate = "creation_date"
        case urgent = "is_urgent"

        case image = "images_url"
    }

    enum ImagesKeys: String, CodingKey {
        case image = "small"
        case thumbnail = "thumb"
    }

    required init(from decoder: Decoder) throws {
        let values = try decoder.container(keyedBy: CodingKeys.self)
        id = try values.decode(Int.self, forKey: .id)
        categoryId = try values.decode(Int.self, forKey: .categoryId)
        title = try values.decode(String.self, forKey: .title)
        description = try values.decode(String.self, forKey: .description)
        price = try values.decode(Double.self, forKey: .price)
        urgent = try values.decode(Bool.self, forKey: .urgent)

        let imagesValues = try values.nestedContainer(keyedBy: ImagesKeys.self, forKey: .image)
        image = try? imagesValues.decode(String.self, forKey: .image)
        thumbnail = try? imagesValues.decode(String.self, forKey: .thumbnail)

        if let date = Ad.dateFormatter.date(from: try values.decode(String.self, forKey: .creationDate)) {
            creationDate = date
        } else {
            throw NSError()
        }
    }
}

extension Ad: Comparable {
    
    static func == (lhs: Ad, rhs: Ad) -> Bool {
        return lhs.id == rhs.id
    }
    
    static func < (lhs: Ad, rhs: Ad) -> Bool {
        return (lhs.urgent != rhs.urgent) ? lhs.urgent : (lhs.creationDate > rhs.creationDate)
    }

    static func > (lhs: Ad, rhs: Ad) -> Bool {
        return (lhs.urgent != rhs.urgent) ? rhs.urgent : (lhs.creationDate < rhs.creationDate)
    }
}
