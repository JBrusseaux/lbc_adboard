//
//  AdCell.swift
//  AdBoard
//
//  Created by Julien Brusseaux on 29/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import UIKit

class AdCell: UICollectionViewCell {
    
    private static let currencyFormatter: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.usesGroupingSeparator = true
        formatter.numberStyle = .currency
        formatter.locale = Locale(identifier: "fr_FR")
        formatter.currencySymbol = "€"
        formatter.usesSignificantDigits = true
        return formatter
    } ()
    
    private static let dateFormatter: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd/MM/yyyy"
        return formatter
    } ()
    
    private let titleLabel = UILabel()
    private let dateLabel = UILabel()
    private let priceLabel = UILabel()
    private let adImageView = UIImageView()
    private let categoryLabel = UILabel()
    private let urgentLabel = UILabel()
    
    private var imageLoadTask: URLSessionTask?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        loadView()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setup(with ad: Ad) {
        titleLabel.text = ad.title
        dateLabel.text = AdCell.dateFormatter.string(from: ad.creationDate)
        priceLabel.text = AdCell.currencyFormatter.string(from: NSNumber(value: ad.price))
        categoryLabel.text = ad.category?.name
        adImageView.image = nil
        urgentLabel.text = "!"
        urgentLabel.isHidden = !ad.urgent
        loadImage(ad.image)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageLoadTask?.cancel()
        imageLoadTask = nil
    }
}

private extension AdCell {
    func loadView() {
        loadConstraints()
        loadStyle()
    }
    
    func loadConstraints() {
        contentView.constrain(to: self)
        contentView.with(titleLabel, dateLabel, adImageView, priceLabel, UIView().with(categoryLabel), urgentLabel)
        adImageView.constrain(to: contentView, attributes: [.leading, .top, .trailing])
        adImageView.widthAnchor.constraint(equalTo: adImageView.heightAnchor).isActive = true
        adImageView.placeOnTop(of: titleLabel, offset: 4)
        titleLabel.constrain(to: contentView, attributes: [.leading, .trailing], constants: [12, 12])
        priceLabel.topAnchor.constraint(greaterThanOrEqualTo: titleLabel.bottomAnchor).isActive = true
        dateLabel.constrain(to: titleLabel, attributes: [.leading])
        dateLabel.constrain(to: priceLabel, attributes: [.bottom])
        priceLabel.constrain(to: contentView, attributes: [.trailing, .bottom], constants: [12, 12])
        categoryLabel.constrain(to: categoryLabel.superview, attributes: [.leading, .top, .centerX, .centerY], constants: [8, 0, 0, 0])
        categoryLabel.superview?.constrain(to: adImageView, attributes: [.bottom, .centerX], constants: [6, 0])
        urgentLabel.constrain(to: contentView, attributes: [.leading, .top], constants: [6, 6])
        urgentLabel.heightAnchor.constraint(equalToConstant: 18).isActive = true
        urgentLabel.widthAnchor.constraint(equalToConstant: 18).isActive = true
    }
    
    func loadStyle() {
        backgroundColor = .white
        layer.cornerRadius = 12
        layer.shadowOffset = CGSize(width: 0, height: 4)
        layer.shadowOpacity = 0.2
        layer.shadowColor = UIColor.black.cgColor
        
        adImageView.backgroundColor = .lightGray
        adImageView.contentMode = .scaleAspectFill
        adImageView.clipsToBounds = true
        adImageView.layer.maskedCorners = [.layerMinXMinYCorner, .layerMaxXMinYCorner]
        adImageView.layer.cornerRadius = 12
        
        titleLabel.numberOfLines = 2
        titleLabel.textColor = .black
        titleLabel.font = UIFont.systemFont(ofSize: 14, weight: .medium)
        
        dateLabel.textColor = .gray
        dateLabel.font = UIFont.systemFont(ofSize: 11, weight: .medium)
        
        priceLabel.font = UIFont.boldSystemFont(ofSize: 16)
        priceLabel.textColor = UIColor(red: 0.1, green: 0.811, blue: 0.117, alpha: 1)
        
        categoryLabel.font = UIFont.boldSystemFont(ofSize: 12)
        categoryLabel.superview?.backgroundColor = UIColor(red: 0.989, green: 0.411, blue: 0.225, alpha: 1)
        categoryLabel.superview?.layer.cornerRadius = 8
        
        urgentLabel.font = categoryLabel.font
        urgentLabel.layer.cornerRadius = 9
        urgentLabel.clipsToBounds = true
        urgentLabel.textAlignment = .center
        urgentLabel.backgroundColor = UIColor(red: 0.968, green: 0.203, blue: 0.215, alpha: 1)
    }
    
    func loadImage(_ path: String?) {
        guard let path = path, let url = URL(string: path) else { return }
        
        if let imageLoadTask = imageLoadTask {
            if imageLoadTask.originalRequest?.url == url {
                imageLoadTask.resume()
            } else {
                imageLoadTask.suspend()
            }
        } else {
            imageLoadTask = URLSession.shared.dataTask(with: url, completionHandler: { (data, _, _) in
                if let data = data {
                    DispatchQueue.main.async { [weak self] in
                        self?.adImageView.image = UIImage(data: data)
                    }
                }
            })
            imageLoadTask?.resume()
        }
    }
}
