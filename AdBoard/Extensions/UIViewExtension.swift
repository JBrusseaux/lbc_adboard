//
//  UIViewExtension.swift
//  AdBoard
//
//  Created by Julien Brusseaux on 29/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import UIKit

extension UIView {
    func constrain(to view2: UIView?,
                    attributes: [NSLayoutConstraint.Attribute] = [.leading, .trailing, .top, .bottom],
                    constants: [CGFloat] = [0, 0, 0, 0]) {
        guard let view2 = view2,
            let commonSuperview = commonSuperview(with: view2) else { return }
        
        self.translatesAutoresizingMaskIntoConstraints = superview == nil
        view2.translatesAutoresizingMaskIntoConstraints = view2.superview == nil
        
        let reversedValues: [NSLayoutConstraint.Attribute] = [.trailing, .bottom]
        let constraints = zip(attributes, constants)
        .map { NSLayoutConstraint(item: self,
                                  attribute: $0.0,
                                  relatedBy: .equal,
                                  toItem: view2,
                                  attribute: $0.0,
                                  multiplier: 1,
                                  constant: reversedValues.contains($0.0) ? -$0.1 : $0.1) }
        constraints.forEach { commonSuperview.addConstraint($0) }
    }
    
    func placeOnTop(of view2: UIView?, offset: CGFloat = 0) {
        guard let view2 = view2 else { return }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        view2.translatesAutoresizingMaskIntoConstraints = false
        self.bottomAnchor.constraint(equalTo: view2.topAnchor, constant: -offset).isActive = true
    }
    
    func placeNext(to view2: UIView?, offset: CGFloat = 0) {
        guard let view2 = view2 else { return }
        
        self.translatesAutoresizingMaskIntoConstraints = false
        view2.translatesAutoresizingMaskIntoConstraints = false
        self.trailingAnchor.constraint(equalTo: view2.leadingAnchor, constant: -offset).isActive = true
    }
    
    private func commonSuperview(with view2: UIView) -> UIView? {
        let superviews = self.superviews()
        return view2.superviews().first(where: { superviews.contains($0) })
    }
    
    private func superviews() -> [UIView] {
        var superviews = [UIView]()
        var view: UIView? = self
        while let v = view {
            superviews.append(v)
            view = v.superview
        }
        return superviews
    }
    
    @discardableResult func with(_ subviews: CVarArg...) -> UIView {
        subviews
            .compactMap { $0 as? UIView }
            .forEach { addSubview($0) }
        return self
    }
}
