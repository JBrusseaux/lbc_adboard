//
//  AdLoader.swift
//  AdBoard
//
//  Created by Julien Brusseaux on 29/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

import Foundation

class AdLoader: AdLoadingProtocol {
    static let shared = AdLoader(adsPath: "https://raw.githubusercontent.com/leboncoin/paperclip/master/listing.json",
                                 categoriesPath: "https://raw.githubusercontent.com/leboncoin/paperclip/master/categories.json")
    
    private(set) var adsSource: URL
    private(set) var categoriesSource: URL
    
    init(adsPath: String, categoriesPath: String) {
        let adsUrl = URL(string: adsPath)
        let categoriesUrl = URL(string: categoriesPath)
        if let adsUrl = adsUrl, let categoriesUrl = categoriesUrl {
            self.adsSource = adsUrl
            self.categoriesSource = categoriesUrl
        } else {
            fatalError("Invalid source provided")
        }
    }
    
    private func loadCategories(_ completion: @escaping (Result<[AdCategory], Error>) -> Void) {
        let task = URLSession.shared.dataTask(with: categoriesSource) { (data, response, error) in
            if let error = error {
                completion(.failure(error))
            } else {
                guard let data = data, let categories = try? JSONDecoder().decode([AdCategory].self, from: data) else {
                    completion(.failure(AdLoadingError.invalidResponse))
                    return
                }
                completion(.success(categories))
            }
        }
        task.resume()
    }
    
    func loadAds(_ completion: @escaping (Result<[Ad], Error>) -> Void) {
        loadCategories { [weak self] result in
            guard let self = self else { return }
            
            switch result {
            case let .failure(error): completion(.failure(error))
            case let .success(categories):
                let task = URLSession.shared.dataTask(with: self.adsSource) { (data, response, error) in
                    if let error = error {
                        completion(.failure(error))
                    } else {
                        guard let data = data, let ads = try? JSONDecoder().decode([Ad].self, from: data) else {
                            completion(.failure(AdLoadingError.invalidResponse))
                            return
                        }
                        ads.forEach { ad in ad.category = categories.first(where: { $0.id == ad.categoryId }) }
                        completion(.success(ads.sorted()))
                    }
                }
                task.resume()
            }
        }
    }
}
