//
//  AdLoadingProtocol.swift
//  AdBoard
//
//  Created by Julien Brusseaux on 29/02/2020.
//  Copyright © 2020 Julien Brusseaux. All rights reserved.
//

protocol AdLoadingProtocol {
    func loadAds(_ completion: @escaping (Result<[Ad], Error>) -> Void)
}

enum AdLoadingError: Error {
    case invalidResponse
}
